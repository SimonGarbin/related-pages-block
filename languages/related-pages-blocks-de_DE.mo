��    !      $  /   ,      �     �  	   �     �  
                  !     0     >     C     X     h  	   �  
   �     �     �     �     �     �     �  	   �     �     �     �          
                    +     1     ?  �  H     �     �     �     �  	   �  
          	   #     -  #   2     V     l     �     �     �     �     �     �     �     �  	   �            
             $     *     1     :     K     R     d                                                                       	                                         !                                        
       ... Alignment Always open Background Border Child pages Content colors Corner radius Link Mark as current page Open by default Open links in a new tab Page item Page title Related Pages Blocks Related pages Sibling pages Text Title Title colors Title tag Width center child pages children left pages related related pages right sibling pages siblings Project-Id-Version: Related Pages Blocks
PO-Revision-Date: 2023-10-27 06:50+0200
Last-Translator: 
Language-Team: Simon Garbin
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e;esc_html__;esc_html_e
X-Poedit-SearchPath-0: blocks/child-pages/block.js
X-Poedit-SearchPath-1: blocks/sibling-pages/block.js
X-Poedit-SearchPath-2: blocks/custom-pages/block.js
X-Poedit-SearchPath-3: blocks/page-item/block.js
X-Poedit-SearchPath-4: related-pages-blocks.php
 ... Ausrichtung Immer offen Hintergrund Randlinie Kindseiten Farben des Inhalts Eckradius Link Als dargestellte Seite kennzeichnen Standardmäßig offen Links in neuem Tab öffnen Seitenelement Seitentitel Verwandte Seiten Blöcke Verwandte Seiten Geschwisterseiten Text Titel Farben des Titels Titel-Tag Breite mittig Kindseiten Kinder links Seiten verwandt verwandte Seiten rechts Geschwisterseiten Geschwister 