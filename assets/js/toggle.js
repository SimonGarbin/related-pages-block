jQuery(document).ready(function ($) {
    $('.related-pages-block:not(.always-open) .related-pages-block-title-wrap').on(
            'click touchstart', function (e) {
                e.stopPropagation();
                e.preventDefault();

                var titleWrap = $(this);
                var parent = titleWrap.parent();
                var content = titleWrap.next();

                parent.toggleClass('is-closed');
                if (parent.hasClass('is-closed')) {
                    content.slideUp(300);
                } else {
                    content.slideDown(300);
                }
            });
});