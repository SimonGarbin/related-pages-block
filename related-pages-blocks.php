<?php

/**
 * Plugin Name: Related Pages Blocks
 * Plugin URI: https://gitlab.com/SimonGarbin/related-pages-blockss
 * Description: ...
 * Version: 1.0.0
 * Author: Simon Garbin
 * Author URI: https://simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: related-pages-blocks
 * Domain Path: /languages

  Copyright 2023 Simon Garbin
  Related Pages Blocks is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Related Pages Blocks is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Related Pages Block. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

function related_pages_blocks_translation_dummy() {
    $plugin_title = __('Related Pages Blocks', 'related-pages-blocks');
    $plugin_description = __('...', 'related-pages-blocks');
}

add_action(
        hook_name: 'init',
        callback: 'related_pages_blocks_load_textdomain'
);

function related_pages_blocks_load_textdomain() {
    load_plugin_textdomain(
            domain: 'related-pages-blocks',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

/*
  ----------------------------------------
  Block registration and rendering
  ----------------------------------------
 */

function related_pages_blocks_get_child_pages() {

    $current_id = isset($_GET['post']) ? $_GET['post'] : get_the_ID();

    $wp_children = array_reverse(get_children(
                    array(
                        'post_parent' => $current_id,
                        'post_status' => 'publish',
                        'orderby' => 'title'
                    )
    ));

    if (!$wp_children) {
        return array();
    }

    $children = array();

    foreach ($wp_children as $wp_child) {
        $children[] = array(
            'related-pages-blocks/page-item',
            array(
                'url' => get_permalink($wp_child->ID),
                'title' => get_the_title($wp_child->ID)
            )
        );
    }

    return $children;
}

function related_pages_blocks_get_sibling_pages() {

    $current_id = isset($_GET['post']) ? $_GET['post'] : get_the_ID();
    $parent = get_post_parent($current_id);
    $parent_id = $parent ? $parent->ID : null;

    if (!$parent_id) {
        return array();
    }

    $wp_siblings = array_reverse(get_children(
                    array(
                        'post_parent' => $parent_id,
                        'post_status' => 'publish',
                        'orderby' => 'title'
                    )
    ));

    if (!$wp_siblings) {
        return array();
    }

    $siblings = array();

    foreach ($wp_siblings as $wp_sibling) {
        $siblings[] = array(
            'related-pages-blocks/page-item',
            array(
                'url' => get_permalink($wp_sibling->ID),
                'title' => get_the_title($wp_sibling->ID),
                'isCurrent' => ($current_id === $wp_sibling->ID),
            )
        );
    }

    return $siblings;
}

add_action(
        hook_name: 'init',
        callback: 'related_pages_blocks_register_blocks'
);

function related_pages_blocks_register_blocks() {

    wp_register_style(
            handle: 'related-pages-blocks-style',
            src: plugins_url('blocks/style.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );

    wp_register_style(
            handle: 'related-pages-blocks-editor-style',
            src: plugins_url('blocks/editor.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );

    wp_register_style(
            handle: 'font-awesome-v6.4.0',
            src: 'https://use.fontawesome.com/releases/v6.4.0/css/all.css',
            deps: array(),
            ver: false,
            media: 'all'
    );

    wp_register_script(
            handle: 'related-pages-blocks-toggle-script',
            src: plugins_url('/assets/js/toggle.js', __FILE__),
            deps: array('jquery'),
            ver: false,
            in_footer: true
    );

    // Child pages block
    $asset_file = include(plugin_dir_path(__FILE__)
            . '/blocks/child-pages/block.asset.php');
    $script_handle = 'related-pages-blocks-child-pages-editor-script';

    wp_register_script(
            handle: $script_handle,
            src: plugins_url('blocks/child-pages/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_localize_script(
            handle: $script_handle,
            object_name: 'childPageItems',
            l10n: related_pages_blocks_get_child_pages()
    );

    wp_set_script_translations(
            handle: $script_handle,
            domain: 'related-pages-blocks',
            path: plugin_dir_path(__FILE__) . 'languages/'
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/blocks/child-pages',
            args: array(
                'api_version' => 2,
                'render_callback' => 'related_pages_blocks_render_callback',
            )
    );

    // Sibling pages block
    $asset_file = include(plugin_dir_path(__FILE__)
            . '/blocks/sibling-pages/block.asset.php');
    $script_handle = 'related-pages-blocks-sibling-pages-editor-script';

    wp_register_script(
            handle: $script_handle,
            src: plugins_url('blocks/sibling-pages/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_localize_script(
            handle: $script_handle,
            object_name: 'siblingPageItems',
            l10n: related_pages_blocks_get_sibling_pages()
    );

    wp_set_script_translations(
            handle: $script_handle,
            domain: 'related-pages-blocks',
            path: plugin_dir_path(__FILE__) . 'languages/'
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/blocks/sibling-pages',
            args: array(
                'api_version' => 2,
                'render_callback' => 'related_pages_blocks_render_callback',
            )
    );

    // Sibling pages block
    $asset_file = include(plugin_dir_path(__FILE__)
            . '/blocks/sibling-pages/block.asset.php');
    $script_handle = 'related-pages-blocks-custom-pages-editor-script';

    wp_register_script(
            handle: $script_handle,
            src: plugins_url('blocks/custom-pages/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_set_script_translations(
            handle: $script_handle,
            domain: 'related-pages-blocks',
            path: plugin_dir_path(__FILE__) . 'languages/'
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/blocks/custom-pages',
            args: array(
                'api_version' => 2,
                'render_callback' => 'related_pages_blocks_render_callback',
            )
    );

    // Page item block
    $asset_file = include(plugin_dir_path(__FILE__)
            . '/blocks/page-item/block.asset.php');
    $script_handle = 'related-pages-blocks-page-item-editor-script';

    wp_register_script(
            handle: $script_handle,
            src: plugins_url('blocks/page-item/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_set_script_translations(
            handle: $script_handle,
            domain: 'related-pages-blocks',
            path: plugin_dir_path(__FILE__) . 'languages/'
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/blocks/page-item',
            args: array(
                'api_version' => 2,
                'render_callback' => 'related_pages_blocks_page_item_render_callback',
            )
    );
}

function related_pages_blocks_render_callback($attributes, $content, $block) {

    if (!isset($attributes['title']) || !$attributes['title']) {
        return '';
    }

    // Anchor and classes
    $anchor = '';

    if (isset($attributes['anchor']) && $attributes['anchor']) {
        $anchor .= sprintf(' id="%s"', esc_attr($attributes['anchor']));
    }

    $classes = array(
        'related-pages-block',
    );

    $block_name = $block->name;
    if ($block_name == 'related-pages-blocks/sibling-pages') {
        $classes[] = 'sibling-pages-block';
    } else if ($block_name == 'related-pages-blocks/child-pages') {
        $classes[] = 'child-pages-block';
    } else if ($block_name == 'related-pages-blocks/custom-pages') {
        $classes[] = 'custom-pages-block';
    }

    if (!isset($attributes['openByDefault']) || !$attributes['openByDefault']) {
        $classes[] = 'is-closed';
    }

    if (isset($attributes['alwaysOpen']) && $attributes['alwaysOpen']) {
        $classes[] = 'always-open';
    }

    if (isset($attributes['bordered']) && $attributes['bordered']) {
        $classes[] = 'has-border';
    }

    if (isset($attributes['className']) && $attributes['className']) {
        $classes[] = $attributes['className'];
    }

    // Gather styles from block attributes
    $styles = array(
        'block' => array(),
        'title-wrap' => array(),
        'title' => array(),
        'content' => array(),
    );

    if (isset($attributes['width']) && $attributes['width']) {
        $styles['block'][] = 'width:' . esc_attr($attributes['width']);
    }

    if (isset($attributes['alignment']) && $attributes['alignment']) {
        if ($attributes['alignment'] === 'center') {
            $styles['block'][] = 'margin-left:auto';
            $styles['block'][] = 'margin-right:auto';
        } else if ($attributes['alignment'] === 'right') {
            $styles['block'][] = 'margin-left:auto';
            $styles['block'][] = 'margin-right:0';
        }
    }

    if (isset($attributes['bordered']) && $attributes['bordered']) {
        if (isset($attributes['titleBgColor']) && $attributes['titleBgColor']) {
            $styles['content'][] = 'border-color:' . esc_attr($attributes['titleBgColor']);
        }
    }

    if (isset($attributes['borderRadius']) && $attributes['borderRadius']) {
        $styles['block'][] = 'border-radius:' . esc_attr($attributes['borderRadius']);
        if ((!isset($attributes['bordered']) || !$attributes['bordered']) && !isset($attributes['contentBgColor'])) {
            $styles['title-wrap'][] = 'border-radius:' . esc_attr($attributes['borderRadius']);
        }
        $styles['content'][] = 'border-bottom-left-radius:' . esc_attr($attributes['borderRadius']);
        $styles['content'][] = 'border-bottom-right-radius:' . esc_attr($attributes['borderRadius']);
    }

    if (isset($attributes['titleFontColor']) && $attributes['titleFontColor']) {
        $styles['title'][] = 'color:' . esc_attr($attributes['titleFontColor']);
    }

    if (isset($attributes['titleBgColor']) && $attributes['titleBgColor']) {
        $styles['title-wrap'][] = 'background:' . esc_attr($attributes['titleBgColor']);
        $styles['title-wrap'][] = 'border-color:' . esc_attr($attributes['titleBgColor']);
    }

    if (isset($attributes['contentFontColor']) && $attributes['contentFontColor']) {
        $styles['content'][] = 'color:' . esc_attr($attributes['contentFontColor']);
    }

    if (isset($attributes['contentBgColor']) && $attributes['contentBgColor']) {
        $styles['content'][] = 'background:' . esc_attr($attributes['contentBgColor']);
    }

    if (isset($attributes['openByDefault']) && !$attributes['openByDefault']) {
        $styles['content'][] = 'display:none';
    }

    // Combine CSS properties to style attribute
    foreach ($styles as $key => $values) {
        if (!empty($values)) {
            $styles[$key] = sprintf(' style="%s;"', implode(';', $values));
        } else {
            $styles[$key] = '';
        }
    }

    // Generate HTML
    $output = sprintf(
            '<div %1$sclass="%2$s"%3$s>',
            $anchor,
            esc_attr(implode(' ', $classes)),
            $styles['block'],
    );

    // Title
    $output .= sprintf(
            '<div class="related-pages-block-title-wrap"%s>',
            $styles['title-wrap'],
    );

    $title_tag = 'span';
    if (isset($attributes['titleTag']) && $attributes['titleTag']) {
        $title_tag = $attributes['titleTag'];
    }
    $output .= sprintf(
            '<%2$s class="related-pages-block-title"%3$s>%1$s</%2$s>',
            $attributes['title'],
            $title_tag,
            $styles['title'],
    );

    if (!isset($attributes['alwaysOpen']) || !$attributes['alwaysOpen']) {
        $output .= sprintf(
                        '<div class="related-pages-block-arrow"%s>',
                        $styles['title'],
                )
                . '<i class="fa fa-angle-down fa-sm"></i>'
                . '</div>';
    }
    $output .= '</div>';

    // List
    $output .= sprintf(
            '<ul class="related-pages-block-list"%s>',
            $styles['content'],
    );

    $new_tab = false;
    if (isset($attributes['openLinksInNewTab']) && $attributes['openLinksInNewTab']) {
        $new_tab = $attributes['openLinksInNewTab'];
    }

    foreach ($block->inner_blocks as $inner_block) {
        $page_item = $inner_block->render();
        if ($new_tab) {
            $page_item = str_replace('<a ', '<a target="blank" ', $page_item);
        }
        $output .= $page_item;
    }

    $output .= '</ul>';

    $output .= '</div>';

    return $output;
}

function related_pages_blocks_page_item_render_callback($attributes) {

    if (!isset($attributes['title']) || !$attributes['title']) {
        return '';
    }

    // Anchor and classes
    $anchor = '';

    if (isset($attributes['anchor']) && $attributes['anchor']) {
        $anchor .= sprintf(' id="%s"', esc_attr($attributes['anchor']));
    }

    $classes = array();

    if (isset($attributes['isCurrent']) && $attributes['isCurrent']) {
        $classes[] = 'is-current';
    }

    if (isset($attributes['className']) && $attributes['className']) {
        $classes[] = $attributes['className'];
    }

    if ($classes) {
        $classes = sprintf(' class="%s"', esc_attr(implode(' ', $classes)));
    } else {
        $classes = '';
    }

    // Generate HTML
    $output = sprintf('<li%s%s>', $anchor, $classes);

    if (isset($attributes['isCurrent']) && $attributes['isCurrent']) {
        $output .= $attributes['title'];
    } else {
        $url = '';
        if (isset($attributes['url']) && $attributes['url']) {
            $url = $attributes['url'];
        }
        $output .= sprintf(
                '<a href="%2$s">%1$s</a>',
                $attributes['title'],
                $url,
        );
    }

    $output .= '</li>';

    return $output;
}
