(function (blocks, element, blockEditor, components, i18n) {

    var registerBlockType = blocks.registerBlockType;
    var el = element.createElement;
    var __ = i18n.__;

    var useBlockProps = blockEditor.useBlockProps;
    var InspectorControls = blockEditor.InspectorControls;
    var RichText = blockEditor.RichText;
    var LinkControl = blockEditor.__experimentalLinkControl;

    var PanelBody = components.PanelBody;
    var BaseControl = components.BaseControl;
    var ToggleControl = components.ToggleControl;

    registerBlockType('related-pages-blocks/page-item', {
        apiVersion: 2,
        title: __('Page item', 'related-pages-blocks'),
        parent: [
            'related-pages-blocks/sibling-pages',
            'related-pages-blocks/child-pages',
            'related-pages-blocks/custom-pages'
        ],
        category: 'widgets',
        icon: 'editor-ul',
        supports: {
            anchor: true
        },
        attributes: {
            'title': {
                type: 'string',
                default: null
            },
            'url': {
                type: 'string',
                default: null
            },
            'isCurrent': {
                type: 'boolean',
                default: false
            },
            'anchor': {
                type: 'string',
                default: null
            }
        },
        edit: function (props) {
            var attributes = props.attributes;
            var setAttributes = props.setAttributes;

            function onChangeTitle(newTitle) {
                setAttributes({title: newTitle || null});
            }
            
            function onChangeUrl(newUrl, post) {
                setAttributes({url: newUrl.url});
            }

            function onRemoveUrl() {
                setAttributes({url: null});
            }

            function onChangeIsCurrent(newIsCurrent) {
                setAttributes({isCurrent: newIsCurrent});
            }

            return el('div', useBlockProps(), [
                el('li', {className: attributes.isCurrent ? 'is-current' : ''},
                        el(RichText, {
                            tagName: 'span',
                            value: attributes.title,
                            placeholder: __('Page title', 'related-pages-blocks'),
                            onChange: onChangeTitle,
                            withoutInteractiveFormatting: true
                        })
                        ),
                el(InspectorControls, {},
                        el(PanelBody, {},
                        el(BaseControl, {
                                        label: __('Link', 'related-pages-blocks'),
                                        className: 'related-pages-block-link-control'
                                    },
                                el(LinkControl, {
                                    value: {url: attributes.url},
                                    onChange: onChangeUrl,
                                    onRemove: onRemoveUrl,
                                    settings: []
                                })
                                ),
                                el(ToggleControl, {
                                    checked: attributes.isCurrent,
                                    label: __('Mark as current page', 'related-pages-blocks'),
                                    onChange: onChangeIsCurrent
                                })
                                )
                        )
            ]);
        },
        save: function () {
            return;
        }
    });
})(
        window.wp.blocks,
        window.wp.element,
        window.wp.blockEditor,
        window.wp.components,
        window.wp.i18n
        );